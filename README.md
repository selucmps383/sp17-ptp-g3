# JavaScript #

This tutorial is meant to give (at the minimum) a basic overview of JavaScript and its associated technologies for the purposes of CMPS 383 at Southeastern.

## What is JavaScript? ##

* Sparing you all the long, boring definitions, JavaScript is essentially a client-side language for use in web development.

* It is useful for its ability to be easy to learn, and efficient at tasks such as:

>Text matching: checking passwords against one another

>Aesthetics: objects being highlighted when clicked

>Object manipulation: text being dynamically changed based on some action

## Sounds great, how do I use it? ##

* To write JavaScript, we first need a place to actually write it! Let's create a simple web page with some JavaScript.

* First, we need a text editor. We recommend [Notepad++](https://notepad-plus-plus.org/repository/7.x/7.3.1/npp.7.3.1.Installer.exe), but the standard Notepad works fine.

* Once you have your text editor open to a blank document, let's get to writing some HTML first.

>![Notepad++](/img/notepad++.png)

* Pretty basic stuff, our HTML will just say "Hello World!" in a small font. We can test this by saving our document, making sure the extension is HTML, and then opening it in a web browser.

>![SavingFile](/img/savingfile.png)

>![HelloWorld](/img/helloworld.png)

* Now we'll add some JavaScript! Let's say we want to change the font size of our text so it isn't so boring.

* Since JavaScript is a client-side scripting language, you can simply place <script></script> tags around your code. We'll also need to add an 'id' attribute to our paragraph tag to easily target it for change. (This is more important in bigger documents, not so much for one tag).

* We'll create a button used to invoke a JavaScript function, and then we'll create the function that will be activated on click.

>![FinalCode](/img/demo1finalcode.png)

* Now let's see how it looks in a browser!

>![HelloWorldEnlarged](/img/helloworldenlarged.png)

* **Note**: The code we placed in the function could've run in-line after the on click attribute of the button. 

* Demo code [here](Demo1.html)


## DOM Manipulation ##

* The DOM, or Document Object Model is created when a web page is loaded into a browser.

* It contains HTML elements and CSS styles that can all be changed or removed by using JavaScript.

* Changing the text like in the demo earlier was an example of DOM manipulation.

## AJAX ##

* AJAX, or "Asynchronous JavaScript and XML" is a technology used in tandem with JavaScript to actively update an element on a web page.

* Prevents having to refresh the page

* The best example to think of is a live thread on [Reddit](https://www.reddit.com/r/live): new comments are shown almost instantly; the feed is updated as soon as the server receives the information (this is done through a combination of AJAX and WebSocket)

* An example of pure AJAX can be seen on [this website](http://vm07.braveheart.selu.edu/~cmps394/testAJAX.php)

* JavaScript (with AJAX) code is [here](viewreviews.php)

* Upvote code is [here](upvote.php)

* Downvote code is the same with minor changes (decrement the value instead of increment) 

## Scoping and Closure ##

* Scope is extremely important to understand as a programmer, and it's no different when approaching JavaScript.

* When creating functions in JavaScript, you must be careful when initializing variables, as their scope is dependent upon their placement.

* Creating a variable 'x' in function1 is great and all, but don't assume you can call that same variable in function2 without throwing an exception.

* **You all understand scope, but what about closures?**

* A closure in JavaScript is what allows us to have multiple private variables assigned to another variable (essentially creating objects)

* In the example below, we can see that the variable add becomes a self-invoking function that returns a function which is allowed to access any variable in the parent scope. (IE: counter)

>![Closure](/img/closure.png)

* With this in mind, you can create a plethora of private variables per function in this same way and use the self-invoking method as an object!

## "this" ##

* Most of you should be familiar with the keyword 'this' in the multitude of programming languages we've used thus far in our college careers.

* JavaScript is a little more context sensitive when it comes to 'this'. To be exact, there are four primary situations where 'this' refers to different things.

>1: When called in a function - "this" will (generally) refer to the window and therefore it's accessible elements 

>![AsAFunction](/img/asafunction.png)

>2: When called as a method - "this" will refer to whatever object is calling it

>![AsAMethod](/img/asamethod.png)

>3: When using "new" to create objects - "this" will act the same as if you were making a constructor for an object

>![AsNew](/img/AsNew.png)

>4: When using the "call" and "apply" methods - "this" will have an explicit value based on parameters provided

>![Call](/img/call.png)

>Apply is similar to call, in that we can set the "this" value explicitly, but it will accept an array of parameters rather than a list like above

* Demo code [here](Demo2.html)

## bind() ##

* Speaking of explicit "this" values, bind() is another method used to guarantee that "this" will refer to the object of choice

* Bound functions are useful in that they retain their "this" value no matter how they are called

* You can nest a bound function and pass it all around the world and your "this" will stay the same object you originally intended it to be

## ES6 ##

* ECMAScript is the client-side scripting language that was based on JavaScript. It is the standardized form of Javascript and widely used by web developers.

* Best way to think of it is that ECMAScript is the language, and JavaScript (and variants) are **dialects**.

* The latest rendition of ECMAScript is version 7, also known as ES7, but the most commonly used is still ES6.

* [Here](http://exploringjs.com/es6/) is a huge reference manual for everything ES6 has to offer, we'll leave you guys to explore it if you so choose.

## Babel ##

* Following up with ES6, we have Babel, which is what is known as a 'transpiler'. This is a special compiler that receives JavaScript as an input, and spits JavaScript back out, but not just any JavaScript, it's actually ECMAScript!

>![Babel](/img/babel.png)

* Advantages of using Babel are evident when referencing the aforementioned ES6 manual. A large multitude of features are opened to you, including:

>Arrow functions: save space by simplifying syntax

>![Arrow](/img/arrow.png)

>Template literals: allows the use of embedded expressions

>![TemplateLiteral](/img/templateliteral.png)

>Destructuring: for extracting data from arrays and objects (even nested ones!)

>![Destructuring](/img/destructuring.png)


## Webpack ##

* Webpack is a nifty build tool for taking all non-code assets (read: images, fonts, etc.) and places them in a **dependency graph**

* This allows you to use 'require()' calls in JavaScript, something you normally cannot do. 

* Once you have configured Webpack with your application, you will have a "bundled" file that can quickly and efficiently pull all required files for use on the application.

* It is more efficient the more non-code assets you have.

## Extra Resources ##

* [W3Schools](http://www.w3schools.com/js/)

* [MDN Reference Guide for JS](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)

* [ES6 Guide](http://exploringjs.com/es6/) that was shown earlier

* [JavaScript Subreddit](https://www.reddit.com/r/javascript/)